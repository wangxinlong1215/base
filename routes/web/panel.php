<?php
Route::domain(env('PANEL_DOMAIN'))->group(function () {
    Route::get('/login', 'Panel\LoginController@index')->name('panel.login');
    Route::post('/login', 'Panel\LoginController@login')->name('panel.landing');
    Route::get('/logout', 'Panel\LoginController@logout')->name('panel.logout');
});