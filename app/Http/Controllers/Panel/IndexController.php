<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Config;

class IndexController extends Controller
{
    public function index()
    {
        return view('panel.index.index', ['menu_list' => Config::get('panel.leftMenu')]);
    }

    public function welcome()
    {
        return view('panel.index.welcome');
    }
}
