<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller {
    public function index() {
        return view('panel.login.index');
    }

    public function login(Request $request) {
        if ($request->isMethod('post')) {
            $username = $request->input('username');
            $password = $request->input('password');
            $credentials = [
                'email' => $username,
                'password' => $password,
                'is_admin' => 'yes'
            ];
            if (Auth::attempt($credentials)) {
                return redirect()->route('panel.index');
            }
            return redirect()->route('panel.login')->withInput();
        }
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('login');
    }
}
