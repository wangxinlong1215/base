<?php

namespace App\Logic;

trait BaseLogic {

    /**
     * 返回当前model对象
     * @return mixed
     */
    public function getThisModel() {
        return $this->model;
    }

    /**
     * 添加
     * @param $input
     * @return mixed
     */
    public function store($input) {
        return $this->save($this->model, $input);
    }

    /**
     * 查询主键，且修改数据
     * @param $id
     * @param $input
     * @return mixed
     */
    public function update($id, $input) {
        $this->model = $this->getById($id);
        return $this->save($this->model, $input);
    }

    public function save($model, $input) {
        $model->fill($input);
        $model->save();
        return $model;
    }

    /**
     * 主键查询
     * @param $id
     * @return mixed
     */
    public function getById($id) {
        return $this->model->findOrFail($id);
    }

    /**
     * 查询第一条符合条件数据
     * @param $where
     * @return mixed
     */
    public function getFirstByWhere($where) {
        return $this->model->where($where)->first();
    }

    /**
     * 查询所有数据
     * @return mixed
     */
    public function listAll() {
        return $this->model->get();
    }

    /**
     * 查询符合条件的所有数据
     * @param        $where
     * @param string $order
     * @param string $sort
     * @return mixed
     */
    public function listByWhere($where, $order = '', $sort = 'DESC') {
        if (empty($order)) {
            return $this->model->where($where)->get();
        } else {
            return $this->model->where($where)->orderBy($order, $sort)->get();
        }
    }

    /**
     * return  paginate list
     *
     * @param int    $pagesize
     * @param string $sort
     * @param string $sortColumn
     * @return mixed
     */
    public function page($where = FALSE, $pagesize = 20, $sortColumn = 'weight', $sort = 'asc') {
        if ($where) {
            if ($sortColumn != 'created_at') {
                return $this->model->where($where)->orderBy($sortColumn, $sort)->orderBy('created_at', 'desc')->paginate($pagesize);
            }
            return $this->model->where($where)->orderBy($sortColumn, $sort)->paginate($pagesize);
        } else {
            return $this->model->orderBy($sortColumn, $sort)->paginate($pagesize);
        }
    }

    /**
     * 主键查询，且删除
     * @param $id
     * @return mixed
     */
    public function destroy($id) {
        return $this->getById($id)->delete();
    }
}